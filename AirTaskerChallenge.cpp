#include "RequestHandler.h"
#include <cpp-httplib/httplib.h>

int main()
{
    httplib::Server svr{};
    RequestHandler handler{5};

    svr.Get("/", [&handler](const httplib::Request & req, httplib::Response & res) {
        handler.ProcessRequest(req, res);
    });

    svr.Put("/", [&handler](const httplib::Request & req, httplib::Response & res) {
        handler.ProcessRequest(req, res);
    });

    svr.Post("/", [&handler](const httplib::Request & req, httplib::Response & res) {
        handler.ProcessRequest(req, res);
    });

    svr.Delete("/", [&handler](const httplib::Request & req, httplib::Response & res) {
        handler.ProcessRequest(req, res);
    });

    svr.listen("localhost", 8080);
}
