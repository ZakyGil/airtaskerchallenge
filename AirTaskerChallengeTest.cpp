// clang-format off
#define CATCH_CONFIG_MAIN
#include <catch2/catch.hpp>
// clang-format on

#include "Request.h"
#include "RequestHandler.h"
#include <chrono>
#include <cpp-httplib/httplib.h>
#include <thread>
#include <iostream>

TEST_CASE("Request is created", "[request]")
{
    // This is just to disable cout logging
    std::cout.setstate(std::ios_base::failbit);

    Request testRequest{"/", Request::RequestType::GET, "HTTP/1.1", httplib::Headers(), "test"};
    auto requestString{testRequest.RequestToString()};
    REQUIRE(requestString.find("Path: /, RequestType: GET") != std::string::npos);
}

TEST_CASE("Request is dead", "[request]")
{
    Request testRequest{"/",    Request::RequestType::GET, "HTTP/1.1", httplib::Headers(),
                        "test", std::chrono::seconds(0)};

    std::this_thread::sleep_for(1s);
    REQUIRE(testRequest.IsDead());
}

TEST_CASE("Request is alive", "[request]")
{
    Request testRequest{"/",    Request::RequestType::GET, "HTTP/1.1", httplib::Headers(),
                        "test", std::chrono::seconds(10)};
    REQUIRE(!testRequest.IsDead());
}

TEST_CASE("RequestHandler processed request", "[requesthandler]")
{
    RequestHandler handler{1, std::chrono::seconds(10)};
    httplib::Request testRequest;
    httplib::Response testResponse;

    handler.ProcessRequest(testRequest, testResponse);
    REQUIRE(testResponse.status == 200);
    REQUIRE(testResponse.body == "Request processed");

    handler.ProcessRequest(testRequest, testResponse);
    REQUIRE(testResponse.status == 429);
    REQUIRE(testResponse.body.find("Rate limit exceeded.Try again in ") != std::string::npos);
}

TEST_CASE("RequestHandler queue size test", "[requesthandler]")
{
    RequestHandler handler{2, std::chrono::seconds(10)};
    httplib::Request testRequest;
    httplib::Response testResponse;

    REQUIRE(handler.GetQueueSize() == 0);

    handler.ProcessRequest(testRequest, testResponse);
    REQUIRE(handler.GetQueueSize() == 1);
}

TEST_CASE("RequestHandler queue purge test", "[requesthandler]")
{
    RequestHandler handler{2, std::chrono::seconds(0)};
    httplib::Request testRequest;
    httplib::Response testResponse;

    REQUIRE(handler.GetQueueSize() == 0);

    handler.ProcessRequest(testRequest, testResponse);

    // This should be suficient to allow the queue to be purged.
    std::this_thread::sleep_for(1s);

    REQUIRE(handler.GetQueueSize() == 0);
}
