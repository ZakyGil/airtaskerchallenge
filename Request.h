#pragma once

#include <chrono>
#include <cpp-httplib/httplib.h>
#include <string>

using namespace std::chrono;

/**
 * \class	Request
 *
 * \brief	A container class to hold relevant request information
 **/
class Request
{
public:
    /**
     * \enum RequestType
     *
     * \brief	An enumerator to store the type of request
     **/
    enum class RequestType
    {
        GET,
        PUT,
        POST,
        DEL,
        INVALID
    };

    /**
     * \brief
     *
     * \param	type	The request type as a string
     * \return The RequestType
     **/
    static RequestType RequestTypeFromString(const std::string & type);

    /**
     * \brief A helper function to convert a RequestType enum to a string
     * \param	type	The request type as an enum
     * \return The type of request in string form
     **/
    static std::string RequestTypeToString(const RequestType type);

public:
    Request() = delete;
    Request(const std::string & path, const RequestType requestType, const std::string & version,
            const httplib::Headers & headers, const std::string & body,
            const seconds & lifetime = seconds(3600));

    ~Request() = default;

    /**
     * \brief Returns the time to live this request has
     *
     * \return std::chrono::seconds The duration in seconds left to live
     **/
    double TimeToLive() const;

    /**
     *  \brief Use the limiting function to check if the request is expired **
     *  \return True if the request has expired
     **/
    bool IsDead() const;

    /**
     * \brief Convert the request to a readable string
     *
     * \return std::string The request as a string
     **/
    std::string RequestToString() const;

private:
    const std::string path{"/"};
    const RequestType type{RequestType::GET};
    const std::string version{"HTTP/1.1"};
    const httplib::Headers headers{};
    const std::string body{""};
    const high_resolution_clock::time_point time{};
    const seconds lifetime{3600};
};
