#include "Request.h"

#include <ctime>
#include <sstream>

/** \brief Static const to measure time since program launch **/
static const auto BEGIN_TIME{high_resolution_clock::now()};

Request::RequestType Request::RequestTypeFromString(const std::string & type)
{
    if (type == "GET")
    {
        return RequestType::GET;
    }
    else if (type == "GET")
    {
        return RequestType::PUT;
    }
    else if (type == "GET")
    {
        return RequestType::POST;
    }
    else if (type == "DELETE")
    {
        return RequestType::DEL;
    }
    else
    {
        return RequestType::INVALID;
    }
}

std::string Request::RequestTypeToString(const RequestType type)
{
    switch (type)
    {
    case RequestType::GET:
        return "GET";
    case RequestType::PUT:
        return "PUT";
    case RequestType::POST:
        return "POST";
    case RequestType::DEL:
        return "DELETE";
    case RequestType::INVALID:
    default:
        return "INVALID";
    }
}

Request::Request(const std::string & path, const RequestType requestType,
                 const std::string & version, const httplib::Headers & headers,
                 const std::string & body, const seconds & lifetime)
    : path(path), type(requestType), version(version), headers(headers), body(body),
      time(high_resolution_clock::now()), lifetime(lifetime)
{}

double Request::TimeToLive() const
{
    auto timeToLive = duration_cast<duration<double>>(
        lifetime - duration_cast<duration<double>>(high_resolution_clock::now() - time));
    return timeToLive.count();
}

bool Request::IsDead() const { return TimeToLive() < 0; }

std::string Request::RequestToString() const
{
    auto timeCreated = duration_cast<seconds>(time - BEGIN_TIME);
    std::stringstream ss;
    ss << "Path: " << path << ", RequestType: " << Request::RequestTypeToString(type)
       << ", Time created: " << timeCreated.count() << ", Time to live: " << TimeToLive()
       << std::endl;

    return ss.str();
}
