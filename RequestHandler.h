#pragma once

#include "Request.h"
#include <chrono>
#include <cpp-httplib/httplib.h>
#include <deque>

/**
 * \class	RequestHandler
 *
 * \brief	A request handler that can impose request limits
 **/
class RequestHandler
{
public:
    /** \brief	Default constructor/destructor are sufficient in this case **/
    RequestHandler(const size_t MAX_REQUESTS = 100,
                   const std::chrono::seconds & REQUEST_LIFETIME = std::chrono::seconds{3600});
    ~RequestHandler() = default;

    /**
     * \brief	Process an incoming request
     * \param	req	The incoming http request object
     * \param	res The response that will be returned
     *
     *	This function will collect the incoming request. If the number of requests do not exceed the
     *  current limit defined by the limiter, it will process the request. If the number of requests
     *  exceed the limit, it will ignore the request and return the error code 429
     **/
    void ProcessRequest(const httplib::Request & req, httplib::Response & res);

    /**
     * \brief Get the size of the request queue
     *
     * \return size_t The size of the queue
     **/
    size_t GetQueueSize();

private:
    /** \brief	The maximum number of requests that are allowed in the queue at one time **/
    const size_t MAX_REQUESTS{5};

    const std::chrono::seconds REQUEST_LIFETIME{3600};

    /**	\brief  The actual deque object used to store serviced requests **/
    std::deque<Request> requestQueue{};

    /** \brief apply the current limiter to the queue and remove any requests which are now out of
     *         the scope of the limiter **/
    void SanitiseQueue();

    /** \brief Helper function to log an incoming request to console **/
    void LogRequest(const httplib::Request & req);

    /** \brief Helper function to log an incoming response to console **/
    void LogResponse(const httplib::Response & res);
};
