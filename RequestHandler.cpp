#include "RequestHandler.h"

#include <iostream>
#include <sstream>

RequestHandler::RequestHandler(const size_t MAX_REQUESTS,
                               const std::chrono::seconds & REQUEST_LIFETIME)
    : MAX_REQUESTS(MAX_REQUESTS), REQUEST_LIFETIME(REQUEST_LIFETIME)
{}

void RequestHandler::ProcessRequest(const httplib::Request & req, httplib::Response & res)
{
    LogRequest(req);

    SanitiseQueue();
    if (requestQueue.size() < MAX_REQUESTS)
    {
        auto requestType{Request::RequestTypeFromString(req.method)};
        requestQueue.emplace_back(req.path, requestType, req.version, req.headers, req.body,
                                  REQUEST_LIFETIME);
        std::cout << "Added request: " << requestQueue.back().RequestToString();
        res.set_content("Request processed", "text/plain");
        res.status = 200;
    }
    else
    {
        std::stringstream ss;
        ss << "Rate limit exceeded.Try again in " << requestQueue.front().TimeToLive()
           << " seconds";
        res.set_content(ss.str(), "text/plain");
        res.status = 429;
    }

    LogResponse(res);
}

size_t RequestHandler::GetQueueSize()
{
    SanitiseQueue();
    return requestQueue.size();
}

void RequestHandler::SanitiseQueue(void)
{
    while (!requestQueue.empty() && requestQueue.front().IsDead())
    {
        requestQueue.pop_front();
    }
}

/** \brief A utility function to split headers into a readable string
 *  \param headers	The headers in a request
 *  \return The headers in a readable string **/
std::string DumpHeaders(const httplib::Headers & headers)
{
    std::string headersString{""};
    for (const auto & header : headers)
    {
        headersString += header.first + ": " + header.second + "\n";
    }

    return headersString;
}

void RequestHandler::LogRequest(const httplib::Request & req)
{
    std::cout << "Request:\n\tmethod: " << req.method << "\n\tpath: " << req.path
              << "\n\theaders: " << DumpHeaders(req.headers) << "\n\tbody: " << req.body
              << "\n\tversion: " << req.version << "\n\ttarget: " << req.target << "\n";
}

void RequestHandler::LogResponse(const httplib::Response & res)
{
    std::cout << "Response:\n\tstatus: " << res.status
              << "\n\theaders: " << DumpHeaders(res.headers) << "\n\tbody: " << res.body
              << "\n\tversion: " << res.version << "\n";
}
